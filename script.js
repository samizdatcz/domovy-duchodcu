
var map = L.map('map', {
  minZoom: 7,
  maxZoom: 13,
  zoom: 8,
  center: [49.78, 15.5],
  maxBounds: [[48.3,11.6], [51.3,19.1]]
});


var baseLayer = L.tileLayer("https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png", {
  zIndex: 1,
  attribution: 'mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'
});
baseLayer.addTo(map);
var labelLayer = L.tileLayer("https://samizdat.cz/tiles/ton_l2/{z}/{x}/{y}.png", {
  zIndex: 3,
  opacity: 0.8
});
labelLayer.addTo(map);
L.Icon.Default.imagePath = "https://samizdat.cz/tools/leaflet/images/";

var createMarker = function(datum, options) {
	var marker = L.marker([datum.Latitude, datum.Longitude], options)
	var popupContent = "<b>" + datum['Název'] + "</b><br>";
	popupContent += datum['Adresa'] + "<br><br>"
	popupContent += "Web: <a href='" + datum['Web'] + " target='_blank'>" + datum['Web'] + "</a><br>";
	popupContent += "Telefon: " + datum['Telefonní kontakt'] + "<br><br>";
	popupContent += "Kapacita: " + datum['Kapacita služby - počet lůžek'] + "<br>";
	popupContent += "Počet klientů na zaměstnance: " + datum['Počet klientů ve službě/Celkové přepočtené úvazky'].substring(0, 4) + "<br>";
	popupContent += "Zaměstnanců s maturitou: " + datum['Procento zaměstnanců s maturitou'].substring(0, 4) + " %<br>";
	marker.bindPopup(popupContent);
	marker.datum = datum;
	return marker;
}

var perWorkerScale = d3.scale.quantize()
		.domain([0, 6.26])
		.range(['#fcbba1','#fc9272','#fb6a4a','#ef3b2c','#cb181d','#a50f15','#67000d']);

var workerEducationScale = d3.scale.quantize()
		.domain([21, 85])
		.range(['#bfd3e6','#9ebcda','#8c96c6','#8c6bb1','#88419d','#810f7c','#4d004b']);

var toFloat = function(str) {
	return parseFloat(str.replace(",", "."));
}

var createMarkerPerWorker = function(datum) {
	var color = perWorkerScale(toFloat(datum['Počet klientů ve službě/Celkové přepočtené úvazky']))
	if(color == undefined) {
		color = "#888";
	}
	var icon = L.divIcon({
		html: "<div style='background-color:" + color +"'></div>",
		iconSize: [20, 20]
	});
	return createMarker(datum, {icon: icon});
}

var createMarkerForWorkerEducation = function(datum) {
	var color = workerEducationScale(toFloat(datum['Procento zaměstnanců s maturitou']))
	if(color == undefined) {
		color = "#888";
	}
	var icon = L.divIcon({
		html: "<div style='background-color:" + color +"'></div>",
		iconSize: [20, 20]
	});
	return createMarker(datum, {icon: icon});
}

d3.tsv("./data/data.tsv", function(row) {
	row.Latitude = parseFloat(row.Latitude);
	row.Longitude = parseFloat(row.Longitude);
	return row;
} ,function(data) {
	data = data.filter(function(datum) {
		return (datum.Latitude && datum.Longitude)
	})
	var markers = data.map(createMarker);
	var baseLayerGroup = L.layerGroup();
	map.addLayer(baseLayerGroup);
	var layers = ["mladší senioři (65 - 80 let)", "starší senioři (nad 80 let)", "duševně chronicky nemocní", "chronicky nemocní", "jinak zdravotně postižení", "kombinovaně postižení", "mentálně postižení", "tělesně postižení", "sluchově postižení", "zdravotně postižení", "zrakově postižení", "osoby v krizi", "rizikově žijící"];
	var layersAssoc = {"všechny domovy důchodců": baseLayerGroup};
	var baseMarkerLayer = L.layerGroup(markers);
	var perWorkerRatioLayer = L.layerGroup(data.map(createMarkerPerWorker));
	var workerEducationLayer = L.layerGroup(data.map(createMarkerForWorkerEducation));
	var baseLayersAssoc = {
		"Podle přijímaných klientů": baseMarkerLayer,
		"Počet klientů na zaměstnance": perWorkerRatioLayer,
		"Podíl zaměstnanců s maturitou": workerEducationLayer,
	};
	map.addLayer(baseMarkerLayer);
	layers.forEach(function(layer) {
		layersAssoc[layer] = L.layerGroup()
	});
	layers.unshift("všechny domovy důchodců");
	L.control.layers(baseLayersAssoc, layersAssoc, {collapsed: false}).addTo(map);
	var anyChange = false;
	var checkboxes = d3.selectAll(".leaflet-control-layers-overlays input");
	map.on("baselayerchange", function() {
		var disabled = map.hasLayer(baseMarkerLayer) ? undefined : true;
		checkboxes.attr("disabled", disabled);
	});
	checkboxes.on("change", function() {
		var usedLayers = layers.filter(function(layer) {
			return map.hasLayer(layersAssoc[layer])
		});
		if(!anyChange) {
			anyChange = true;
			checkboxes[0][0].checked = false;
			usedLayers.shift();
		}
		var usedLayersLen = usedLayers.length;
		if (usedLayersLen == 0) {
			markers.forEach(function(marker) {
				baseMarkerLayer.addLayer(marker);
			})
		} else if(usedLayers.indexOf("všechny domovy důchodců") !== -1) {
			markers.forEach(function(marker) {
				baseMarkerLayer.addLayer(marker);
			})
		} else {
			markers.forEach(function(marker) {
				for(var i = 0; i < usedLayersLen; i++) {
					if(marker.datum[usedLayers[i]] != "ano") {
						baseMarkerLayer.removeLayer(marker);
						return;
					}
				}
				baseMarkerLayer.addLayer(marker);
			})
		}
	})
})

